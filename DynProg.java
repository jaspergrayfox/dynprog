import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

class DynProg {
    private static void printIfNine(
        int i,
        String newKey,
        int newValue,
        int target,
        LinkedHashMap<Integer, LinkedHashMap<String, Integer>> cache) {
        if (i == 9 && newValue == target) {
            System.out.println(String.format("%s = %d", newKey, target));
        } else {
            cache.get(i).put(newKey, newValue);
        }
    }

    // e.g. computes a new value for 1 + 234 given 1 + 23 and 4
    private static int processConcat(
        int i,
        String key,
        int value,
        LinkedHashMap<Integer, LinkedHashMap<String, Integer>> cache) {
        int lastSpaceIndex = key.lastIndexOf(' ');
        if (lastSpaceIndex == -1) {
            return value * 10 + i;
        } else {
            char prevChar = key.charAt(lastSpaceIndex - 1);
            String afterSpaceVal = key.substring(lastSpaceIndex + 1);
            int numAfterSpace = Integer.parseInt(afterSpaceVal);
            int newNum = numAfterSpace * 10 + i;
            if (prevChar == '-') {
                return value + numAfterSpace - newNum;
            } else {
                return value - numAfterSpace + newNum;
            }
        }
    }

    private static int getNumFromUser() {
        int n = -1;
        String unparsed;
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.print("Enter a base-10 number: ");
            unparsed = scanner.nextLine().trim();

            try {
                n = Integer.parseInt(unparsed);
                break;
            } catch (NumberFormatException e) {
                System.out.println("Input given was not a base-10 number."); 
            }
        }
        return n;
    }

    public static void main(String[] args) {
        int target = getNumFromUser();
        
        // Example: 2 is mapped to 12 -> 12, 1 + 2 -> 3, and 1 - 2 -> -1
        LinkedHashMap<Integer, LinkedHashMap<String, Integer>> cache = new LinkedHashMap<>(16);

        // We are given digits 1 to 9
        // We must use all digits and find all solutions
        // Start with first number and cache the results
        cache.put(1, new LinkedHashMap<String, Integer>());
        cache.get(1).put("1", 1);

        for (int i = 2; i <= 9; i++) {
            cache.put(i, new LinkedHashMap<String, Integer>(8192));
            LinkedHashMap<String, Integer> previousCache = cache.get(i - 1);
            for (Map.Entry<String, Integer> entry : previousCache.entrySet()) {
                String key = entry.getKey();
                int value = entry.getValue();
                printIfNine(i, key + " + " + i,
                    value + i, target, cache);
                printIfNine(i, key + " - " + i,
                    value - i, target, cache);
                printIfNine(i, key + i,
                    processConcat(i, key, value, cache), target, cache);
            }
        }
    }
}
