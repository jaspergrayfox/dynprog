function printIfNine(i, newKey, newValue, target, cache) {
    if (i === 9 && newValue === target) {
        console.log(`${newKey} = ${newValue}`);
    } else {
        cache.get(i).set(newKey, newValue);
    }
}

// e.g. computes a new value for 1 + 234 given 1 + 23 and 4
function processConcat(i, key, value, cache) {
    const lastSpaceIndex = key.lastIndexOf(' ');
    if (lastSpaceIndex === -1) {
        // We're directly concatenating
        return value * 10 + i;
    } else {
        const prevChar = key[lastSpaceIndex - 1];
        const afterSpaceVal = key.substring(lastSpaceIndex + 1);
        const numAfterSpace = parseInt(afterSpaceVal, 10);
        const newNum = numAfterSpace * 10 + i;
        if (prevChar === '-') {
            return value + numAfterSpace - newNum;
        } else {
            return value - numAfterSpace + newNum;
        }
    }
}

function isNumber(buf) {
    if (!buf.length) {
        return false;
    }
    for (let i = 0; i < buf.length; i++) {
        if (buf[i] === '-' && i === 0 && buf.length > 1) {
            continue;
        }
        if (buf[i] < '0' || buf[i] > '9') {
            return false;
        }
    }
    return true;
}

function getNumFromUser() {
    return new Promise((resolve) => {
        process.stdout.write('Enter a base-10 number: ');
        process.stdin.on('data', (buf) => {
            let s = buf.toString('utf8');
            if (!s.length) {
                return;
            }
            s = s.trim();
            if (!isNumber(s)) {
                process.stdout.write('Input given was not a base-10 number.\n');
                process.stdout.write('Enter a base-10 number: ');
            } else {
                resolve(parseInt(s, 10));
            }
        });
    });
}

// Main starts here
getNumFromUser().then(target => {
    const cache = new Map();

    // We are given digits 1 to 9
    // We must use all digits and find all solutions
    // Start with first number and cache the results
    cache.set(1, new Map());
    cache.get(1).set('1', 1);

    for (let i = 2; i <= 9; i++) {
        cache.set(i, new Map());
        const previousCache = cache.get(i - 1);
        const iStr = '' + i;
        for (const [key, value] of previousCache) {
            printIfNine(i, key + ' + ' + iStr,
                value + i, target, cache);
            printIfNine(i, key + ' - ' + iStr,
                value - i, target, cache);
            printIfNine(i, key + iStr,
                processConcat(i, key, value, cache), target, cache);
        }
    }
    process.exit(0);
});
