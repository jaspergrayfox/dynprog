def print_if_nine(i, new_key, new_value, target, cache):
    if i == 9 and new_value == target:
        print(f"{new_key} = {new_value}")
    else:
        cache[i][new_key] = new_value

# e.g. computes a new value for 1 + 234 given 1 + 23 and 4
def process_concat(i, key, value, cache):
    last_space_index = key.rfind(" ")
    if last_space_index == -1:
        # We're directly concatenating
        return value * 10 + i
    else:
        prev_char = key[last_space_index - 1]
        after_space_val = key[last_space_index + 1:]
        num_after_space = int(after_space_val)
        new_num = num_after_space * 10 + i
        if prev_char == '-':
            return value + num_after_space - new_num
        else:
            return value - num_after_space + new_num

def get_num_from_user():
    n = -1
    while True:
        print("Enter a base-10 number: ", end="")
        unparsed = input()
        try:
            n = int(unparsed)
            return n
        except:
            print("Input given was not a base-10 number.")

# Main starts here
target = get_num_from_user()
cache = dict()

# We are given digits 1 to 9
# We must use all digits and find all solutions
# Start with first number and cache the results
cache[1] = dict()
cache[1]["1"] = 1

for i in range(2, 10):
    cache[i] = dict()
    previous_cache = cache[i - 1]
    str_i = str(i)
    for (key, value) in previous_cache.items():
        print_if_nine(i, key + " + " + str_i,
            value + i, target, cache)
        print_if_nine(i, key + " - " + str_i,
            value - i, target, cache)
        print_if_nine(i, key + str_i,
            process_concat(i, key, value, cache), target, cache)
