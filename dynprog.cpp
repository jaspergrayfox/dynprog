#include <iostream>
#include <map>
#include <string>

void print_if_nine(
    int i,
    const std::string& new_key,
    int new_value,
    int target,
    std::map<int, std::map<std::string, int>>& cache) {
    if (i == 9 && new_value == target) {
        std::cout << new_key << " = " << target << std::endl;
    } else {
        cache[i][new_key] = new_value;
    }
}

// e.g. computes a new value for 1 + 234 given 1 + 23 and 4
int process_concat(
    int i,
    const std::string& key,
    int value,
    const std::map<int, std::map<std::string, int>>& cache) {
    // Get the last number in the key
    const std::string::size_type last_space_index = key.rfind(" ");
    if (last_space_index == std::string::npos) {
        // We're directly concatenating
        return value * 10 + i;
    } else {
        // Get character before last space
        const char prev_char = key[last_space_index - 1];
        // Also get value of string after space
        const std::string after_space_val = key.substr(last_space_index + 1);
        const int num_after_space = std::atoi(after_space_val.c_str());
        const int new_num = num_after_space * 10 + i;
        if (prev_char == '-') {
            return value + num_after_space - new_num;
        } else {
            return value - num_after_space + new_num;
        }
    }
}

bool is_number(const std::string& s) {
    for (size_t i = 0; i < s.size(); i++) {
        const char c = s[i];
        if (i == 0 && c == '-' && s.size() > 1) {
            continue;
        }
        if (c < '0' || c > '9') {
            return false;
        }
    }
    return true;
}

int get_num_from_user() {
    int n = -1;
    std::string unparsed;

    while (true) {
        std::cout << "Enter a base-10 number: ";
        // TODO: Change this to get a line every time
        std::cin >> unparsed;

        if (unparsed.empty() || !is_number(unparsed)) {
            std::cout << "Input given was not a base-10 number." << std::endl;
            continue;
        }

        n = std::atoi(unparsed.c_str());
        break;
    }
    return n;
}


int main() {
    const int target = get_num_from_user();
    
    // Example: 2 is mapped to 12 -> 12, 1 + 2 -> 3, and 1 - 2 -> -1
    std::map<int, std::map<std::string, int>> cache;

    // We are given digits 1 to 9
    // We must use all digits and find all solutions
    // Start with first number and cache the results
    cache[1] = std::map<std::string, int>();
    cache[1]["1"] = 1;

    for (int i = 2; i <= 9; i++) {
        cache[i] = std::map<std::string, int>();
        const std::map<std::string, int> &previous_cache = cache[i - 1];
        for (const auto &[key, value] : previous_cache) {
            print_if_nine(i, key + " + " + std::to_string(i),
                value + i, target, cache);
            print_if_nine(i, key + " - " + std::to_string(i),
                value - i, target, cache);
            print_if_nine(i, key + std::to_string(i),
                process_concat(i, key, value, cache), target, cache);
        }
    }
}
